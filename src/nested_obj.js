import React from 'react'
import { nestedObjCreate } from './nested_obj'
import { connect } from 'react-redux'
import { contentChangeName } from './actions'

const NestedObj = ({name, onChange}) => {
  const onCheck = (event) => {
    if (event.target.checked) {
      return onChange({id: 1, name: 'New Shit'})
    } else {
      return onChange({id: 1, name: 'Old Shit'})
    }
  }

  return (
    <div>
      <h1>NestedObj</h1>
      <input type="checkbox" id="nestedObj" name="nestedObj" onChange={onCheck} />
      <label>ID 1:</label>
      <input type="text" value={name} />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return (
    {name: state.nestedObj.contents.filter(content => { return content.id === 1})[0].name }
  )
}

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: ({id, name}) => { dispatch(contentChangeName({id: id, name: name})) }
  }
}

/*
 * Fool around with differnt method orders.
 */
export default connect(mapStateToProps, mapDispatchToProps)(NestedObj)
