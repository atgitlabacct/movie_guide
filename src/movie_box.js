import React from 'react'
import { connect } from 'react-redux'
import Checkbox from './checkbox'
import { onFavoriteMovie, unFavoriteMovie} from './actions'

const MovieBox = ({movie, favorite, onFavorite, unFavorite}) => {
  const onChange = (checked) => {
    if (checked) {
      onFavorite(movie.title)
    } else {
      unFavorite(movie.title)
    }
  }

  return (
    <div className="h4 mt3 pa3 flex flex-column justify-between ba b--dashed">
      <h3 className="mt0 mb3">{movie.title}</h3>
      <Checkbox
        name="addToFavorite"
        id="addToFavorite"
        label="Favorite"
        checked={favorite}
        onChange={onChange}
      />
    </div>
  )
}

/*
 * Map the state that passed in from the container to the
 * props of the Component passed into the connect function
 * imported from react-redux.
 */
const mapStateToProps = (state, ownProps) => {
  return {
    favorite: state.favorites.includes(ownProps.movie.title)
  }
}

/*
 * Maps the functions (onFavorite, unFavorite) to the props
 * of a component passed into the connect function of react-redux
 */
const mapDispatchToProps = (dispatch) => {
  return {
    onFavorite: (movieTitle) => { dispatch(onFavoriteMovie(movieTitle)) },
    unFavorite: (movieTitle) => { dispatch(unFavoriteMovie(movieTitle)) }
  }
}

/*
 * Without this mapping nothing would be wired up to the Provider (store)
 * of Redux
 */
export default connect(mapStateToProps, mapDispatchToProps)(MovieBox)
