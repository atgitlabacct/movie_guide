import { createStore } from 'redux'
import { devToolsEnhancer } from 'redux-devtools-extension'

/*
 * This imports all our reducers from the exported function call
 * in reducers.js from combineReducers({}).  Also sets up the state
 * for the Redux store because 'Reducers return new state objects'.
 * 
 * Notive in the reducers file all the functions define a default
 * state value in the calls.
 */
import rootReducer from './reducers'

/*
 * Create the store and any middleware
 */
export default createStore(
  rootReducer,
  devToolsEnhancer()
)
