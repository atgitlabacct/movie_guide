export const FILTER_CHANGED = 'FILTER_CHANGED'
export const MOVIES_LOADED = 'MOVIES_LOADED'
export const FAVORITED = 'FAVORITED'
export const UNFAVORITED = 'UNFAVORITED'
export const CONTENT_FILTER = 'CONTENT_FILTER'
export const CONTENT_CHANGE_NAME = 'CONTENT_CHANGE_NAME'

/*
 * ActionCreators
 *
 * What I like about this is it hides the 'type' of action.  So if we ever
 * wanted to change the action names its hidden from the external calls to
 * the ActionCreators.
 *
 * I can change FAVORITED to FAVORITED_MOVIES and have less places besides
 * the reducers to change to the new format.
 */
const onFavoriteMovie= (title) => {
  return {type: FAVORITED, movieTitle: title}
}

const unFavoriteMovie = (title) => {
  return {type: UNFAVORITED, movieTitle: title}
}

const changeFilter = (filter) => {
  return { type: FILTER_CHANGED, filter}
}

const moviesLoaded = (movies) => {
  return { type: MOVIES_LOADED, movies }
}

const contentChangeName= ({id, name}) => {
  return { type: CONTENT_CHANGE_NAME, payload: {id, name}}
}

export { onFavoriteMovie, unFavoriteMovie, changeFilter, moviesLoaded, contentChangeName}
