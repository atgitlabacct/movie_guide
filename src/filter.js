import { connect } from 'react-redux'
import { changeFilter } from './actions'
import Checkbox from './checkbox'

const mapStateToProps = (state) => {
  return {
    checked: state.filter
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: filter => {
      dispatch(changeFilter(filter))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkbox)
