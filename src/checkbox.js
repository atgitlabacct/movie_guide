import React from 'react';

const CheckBox = ({checked, onChange, name, label, id}) => {
  /*
   * This simply wraps the onChange function passed in otherwise the passed
   * in function would need to know about the whole event object.
   *
   * In this scenario the only thing the onChange funciton needs to know about
   * is the target value which now we control what is passed into it.  So the
   * original function passed in is more generic.
   */
  const onCheck = (event) => {
    onChange(event.target.checked)
  }

  return(
    <div className="flex mw4">
      <label className="pr2" htmlFor={id}>{label}</label>
      <input type="checkbox" name={name} id={id} onChange={onCheck} checked={checked} />
    </div>
  )
}

export default CheckBox
