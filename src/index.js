import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import { moviesLoaded, contentChangeName } from './actions'
import { requestMovies} from './movies_api'
import MovieList from './movie_listing'
import Filter from './filter'
import NestedObj from './nested_obj'

/*
 * Notive here we first create the state by dispatching
 * an Action to the reducers.
 *
 * Comment the below out and see what happens.
 * What does the state look like when using the Chrome
 * extension for Redux.
 */
requestMovies()
.then( (movies) => {
  store.dispatch(moviesLoaded(movies))
})

/*
 *
 *  If you Commented the above you you will notice
 *  that the Redux state is created by the @@INIT action.
 *  WE NEVER called the movies to fill the movies reducer,
 *  but the reducer was called by the @@INIT action.
 *
 *  Another experiment might be to not return the state
 *  in some of the reducer case statements...see what happens.
 *  If you do this make sure to uncomment the requestMovies()
 *  promise execution.
 *
 *  If you did experiment #2 you will notice the app completely
 *  fails because the reducers HAVE to return something to create
 *  the state.  What if you return just junk?
 *
 *  What if you return an empty array in the default case for
 *  the movies reducer?
*/

const movieLists = ['Monday', 'Tuesday'].map( (date) => {
  return <MovieList key={date} date={date} />
})

ReactDOM.render(
  <Provider store={store}>
    <main className="ph6 pv4">
      <h1 className="mt0">Programmer</h1>
      <Filter name="filter" id="filter" label="Just favorites" />
      <div className="flex flex-row">
        {movieLists}
      </div>
      <NestedObj />
    </main>
  </Provider>,
  document.getElementById('app')
)
