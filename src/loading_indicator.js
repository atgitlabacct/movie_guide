import React from 'react'
import { connect } from 'react-redux'

const LoadingIndicator = ({loading, children}) => {
  if (loading) {
    return <div>Loading...</div>
  } else {
    return (
      <div>
        {children}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    loading: state.loading
  }
}

export default connect(mapStateToProps)(LoadingIndicator)
