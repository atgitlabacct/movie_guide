import { combineReducers } from 'redux'

import {
  FILTER_CHANGED,
  MOVIES_LOADED,
  FAVORITED,
  UNFAVORITED,
  CONTENT_CHANGE_NAME
} from './actions'

const movies = (state = [], action) => {
  switch(action.type) {
    case MOVIES_LOADED:
      return action.movies
    default:
      return state;
  }
}

const loading = (state = true, action) => {
  switch(action.type) {
    case MOVIES_LOADED:
      return false
    default:
      return state
  }
}

const filter = (state = false, action) => {
  switch(action.type) {
    case FILTER_CHANGED:
      return action.filter
    default:
      return state
  }
}

const favorites = (state = [], action) => {
  switch(action.type) {
    case FAVORITED:
      return [...state, action.movieTitle]
    case UNFAVORITED:
      return state.filter(id => id !== action.movieTitle)
    default:
      return state
  }
}

const defaultNestedState = {
  contents: [
    {
      id: 1,
      name: 'One'
    },
    {
      id: 2,
      name: 'Two'
    }
  ]
}

/*
 * Note that I just created this to fool around with a nested store
 * and really the 'what purpose is this for' is nothing...just here
 * to fool around.
 */
const nestedObj = (state = defaultNestedState, action) => {
  switch(action.type) {
    case CONTENT_CHANGE_NAME:
      const newContents = state.contents.map( (content) => {
        if (action.payload.id === content.id) {
          return Object.assign({}, content, {name: action.payload.name})
        } else {
          return content
        }
      })
      return {...state, ...{contents: newContents}}
    default:
      return state
  }
}

export default combineReducers({movies, filter, favorites, loading, nestedObj})
